from django import forms
from pedido.models import Pedido, ItensPedido
from django.forms.models import inlineformset_factory


class FormPedido(forms.ModelForm):
    class Meta:
        model = Pedido
        fields = '__all__'

class FormItensPedido(forms.ModelForm):
    class Meta:
        model = ItensPedido
        fields = '__all__'  


FormSetItensPedido = inlineformset_factory(
    Pedido, ItensPedido, form=FormItensPedido, extra=0, can_delete=True, min_num=0, validate_min=True
)