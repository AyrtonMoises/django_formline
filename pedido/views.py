from django.shortcuts import render, redirect, get_object_or_404, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib import messages

from pedido.forms import FormPedido, FormSetItensPedido
from pedido.models import Pedido


def criar_pedido(request):
    form = FormPedido(request.POST or None)

    if request.method == 'POST':
        if form.is_valid():
            form.save()
            return redirect('lista_pedido')
        else:
            return render(request, 'pedido.html', {'form': form})

    return render(request, 'pedido.html', {'form': form})


def atualiza_pedido(request, pk):
    pedido = get_object_or_404(Pedido, pk=pk)
    form = FormPedido(request.POST or None, instance=pedido)
    form_set_itens = FormSetItensPedido(request.POST or None, instance=pedido)
    
    if request.method == 'POST':

        if form.is_valid() and form_set_itens.is_valid():
            form.save()
            form_set_itens.save()
            return redirect('atualiza_pedido', pk=pk)
        else:
            return render(request, 'pedido.html', {'form': form, 'form_itens': form_set_itens})
  
    return render(request, 'pedido.html', {'form': form, 'form_itens': form_set_itens})


def lista_pedido(request):
    form_modal = FormPedido()
    pedidos = Pedido.objects.all()
    return render(request, 'lista_pedidos.html', {'pedidos': pedidos, 'form_modal': form_modal})

@csrf_exempt
def edita_pedido_modal(request, pk):
    pedido = get_object_or_404(Pedido, pk=pk)
    form = FormPedido(request.POST or None, instance=pedido)
    
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            return redirect('lista_pedido')
        else:
            messages.error(request, 'Formulário inválido!')
            return redirect('lista_pedido')

    return HttpResponse(form.as_p())