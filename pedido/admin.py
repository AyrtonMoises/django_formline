from django.contrib import admin

from .models import Pedido, ItensPedido


admin.site.register([Pedido, ItensPedido])
