from django.db import models


class Pedido(models.Model):
    status = models.IntegerField()


class ItensPedido(models.Model):
    pedido = models.ForeignKey(Pedido, on_delete=models.CASCADE, related_name='itens_pedido')
    prato = models.CharField(max_length=150)
    quantidade = models.PositiveSmallIntegerField()