from django.urls import path
from pedido.views import criar_pedido, lista_pedido, atualiza_pedido, edita_pedido_modal


urlpatterns = [
    path('criar_pedido/', criar_pedido, name='criar_pedido'),
    path('edita_pedido_modal/<int:pk>/', edita_pedido_modal, name='edita_pedido_modal'),
    path('atualiza_pedido/<int:pk>/', atualiza_pedido, name='atualiza_pedido'),
    path('', lista_pedido, name='lista_pedido'),
]
